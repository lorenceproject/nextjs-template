"use strict";
(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 642:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(689);
;// CONCATENATED MODULE: external "prop-types"
const external_prop_types_namespaceObject = require("prop-types");
;// CONCATENATED MODULE: ./components/Layout/components/Header/TopBar/TopBar.jsx



function TopBar() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "top-bar",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row d-flex justify-content-between align-items-center",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "dlab-topbar-left",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                            className: "la la-phone-volume"
                                        }),
                                        " +00 888 6668811"
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                            className: "las la-map-marker"
                                        }),
                                        " 1073 W Stephen Ave, Clawson"
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "dlab-topbar-right",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                            className: "la la-clock"
                                        }),
                                        "  Mon - Sat 8.00 - 18.00"
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                            className: "las la-envelope-open"
                                        }),
                                        " info@example.com"
                                    ]
                                })
                            ]
                        })
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Header/NavBar/NavBar.jsx


function NavBar() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "sticky-header main-bar-wraper navbar-expand-lg",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "main-bar clearfix ",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "container clearfix",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "logo-header mostion logo-dark",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                            href: "index.html",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "images/logo.png",
                                alt: ""
                            })
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                        className: "navbar-toggler collapsed navicon justify-content-end",
                        type: "button",
                        "data-toggle": "collapse",
                        "data-target": "#navbarNavDropdown",
                        "aria-controls": "navbarNavDropdown",
                        "aria-expanded": "false",
                        "aria-label": "Toggle navigation",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                            })
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "extra-nav",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "extra-cell",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                    id: "quik-search-btn",
                                    type: "button",
                                    className: "site-button-link",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                        className: "la la-search"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    href: "#!",
                                    className: "site-button btnhover13",
                                    children: "Apply Now"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "dlab-quik-search ",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                            action: "#",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                    name: "search",
                                    defaultValue: "",
                                    type: "text",
                                    className: "form-control",
                                    placeholder: "Type to search"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                    id: "quik-search-remove",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                        className: "ti-close"
                                    })
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "header-nav navbar-collapse collapse justify-content-end",
                        id: "navbarNavDropdown",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "logo-header d-md-block d-lg-none",
                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    href: "index.html",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                        src: "images/logo.png",
                                        alt: ""
                                    })
                                })
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                className: "nav navbar-nav",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        className: "active",
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Home",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "sub-menu right",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index.html",
                                                            children: "Home - University"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-2.html",
                                                            children: "Home - Kindergarten"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-3.html",
                                                            children: "Home - Collage"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-4.html",
                                                            children: "Home - Coaching"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-5.html",
                                                            children: "Home - School"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-6.html",
                                                            children: "Home - Online Courese"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-7.html",
                                                            children: "Home - Language School"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "index-8.html",
                                                            children: "Home - Kids School"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Features",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "sub-menu tab-content",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                href: "#!",
                                                                children: [
                                                                    "Header Light ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                        className: "fa fa-angle-right"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "sub-menu",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-1.html",
                                                                            children: "Header 1"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-2.html",
                                                                            children: "Header 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-3.html",
                                                                            children: "Header 3"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-4.html",
                                                                            children: "Header 4"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-5.html",
                                                                            children: "Header 5"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-6.html",
                                                                            children: "Header 6"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                href: "#!",
                                                                children: [
                                                                    "Header Dark ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                        className: "fa fa-angle-right"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "sub-menu",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-1.html",
                                                                            children: "Header 1"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-2.html",
                                                                            children: "Header 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-3.html",
                                                                            children: "Header 3"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-4.html",
                                                                            children: "Header 4"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-5.html",
                                                                            children: "Header 5"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "header-style-dark-6.html",
                                                                            children: "Header 6"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                href: "#!",
                                                                children: [
                                                                    "Footer ",
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                        className: "fa fa-angle-right"
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "sub-menu",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-1.html",
                                                                            children: "Footer 1 "
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-2.html",
                                                                            children: "Footer 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-3.html",
                                                                            children: "Footer 3"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-4.html",
                                                                            children: "Footer 4"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-5.html",
                                                                            children: "Footer 5"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-6.html",
                                                                            children: "Footer 6"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-7.html",
                                                                            children: "Footer 7"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-8.html",
                                                                            children: "Footer 8"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-9.html",
                                                                            children: "Footer 9"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-10.html",
                                                                            children: "Footer 10"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-11.html",
                                                                            children: "Footer 11"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "footer-12.html",
                                                                            children: "Footer 12"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        className: "has-mega-menu",
                                        children: [
                                            " ",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Pages",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "mega-menu",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Pages"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "about-1.html",
                                                                            children: "About us 1"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "about-2.html",
                                                                            children: "About us 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "services-1.html",
                                                                            children: "Services 1"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "services-2.html",
                                                                            children: "Services 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "faq-1.html",
                                                                            children: "Faqs "
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Pages"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "teacher.html",
                                                                            children: "Teachers"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "teachers-profile.html",
                                                                            children: "Teachers Profile"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "courses.html",
                                                                            children: "Courses"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "courses-details.html",
                                                                            children: "Courses Details"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "event.html",
                                                                            children: "Events"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Pages"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "event-details.html",
                                                                            children: "Events Details"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "help-desk.html",
                                                                            children: "Help Desk"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "privacy-policy.html",
                                                                            children: "Privacy Policy"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "error-404.html",
                                                                            children: "Error 404"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "error-405.html",
                                                                            children: "Error 405"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Pages"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "gallery-grid-2.html",
                                                                            children: "Gallery Grid 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "gallery-grid-3.html",
                                                                            children: "Gallery Grid 3"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "gallery-grid-4.html",
                                                                            children: "Gallery Grid 4"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "coming-soon-1.html",
                                                                            children: "Coming Soon 1"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "coming-soon-2.html",
                                                                            children: "Coming Soon 2"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Shop",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "sub-menu",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop.html",
                                                            children: "Shop"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-sidebar.html",
                                                            children: "Shop Sidebar"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-product-details.html",
                                                            children: "Product Details"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-cart.html",
                                                            children: "Cart"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-wishlist.html",
                                                            children: "Wishlist"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-checkout.html",
                                                            children: "Checkout"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-login.html",
                                                            children: "Login"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "shop-register.html",
                                                            children: "Register"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        className: "has-mega-menu",
                                        children: [
                                            " ",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Blog",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "mega-menu",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Blog"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-half-img.html",
                                                                            children: "Half image"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-half-img-sidebar.html",
                                                                            children: "Half image sidebar"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-half-img-left-sidebar.html",
                                                                            children: "Half image sidebar left"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-large-img.html",
                                                                            children: "Large image"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Blog"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-large-img-sidebar.html",
                                                                            children: "Large image sidebar"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-large-img-left-sidebar.html",
                                                                            children: "Large image sidebar left"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-2.html",
                                                                            children: "Grid 2"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-2-sidebar.html",
                                                                            children: "Grid 2 sidebar"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Blog"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-2-sidebar-left.html",
                                                                            children: "Grid 2 sidebar left"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-3.html",
                                                                            children: "Grid 3"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-3-sidebar.html",
                                                                            children: "Grid 3 sidebar"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-3-sidebar-left.html",
                                                                            children: "Grid 3 sidebar left"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Blog"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-grid-4.html",
                                                                            children: "Grid 4"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-single.html",
                                                                            children: "Single"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-single-sidebar.html",
                                                                            children: "Single sidebar"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                            href: "blog-single-left-sidebar.html",
                                                                            children: "Single sidebar right"
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        className: "has-mega-menu",
                                        children: [
                                            " ",
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Element",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "mega-menu",
                                                children: [
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Element"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-buttons.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-mouse"
                                                                                }),
                                                                                " Buttons"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-icon-box-styles.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-grid2"
                                                                                }),
                                                                                " Icon box styles"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-pricing-table.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-grid2-thumb"
                                                                                }),
                                                                                " Pricing table"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-toggles.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-accordion-separated"
                                                                                }),
                                                                                " Toggles"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-team.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-user"
                                                                                }),
                                                                                " Team"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-animation-effects.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layers-alt"
                                                                                }),
                                                                                " Animation Effects"
                                                                            ]
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Element"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-carousel-sliders.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-slider"
                                                                                }),
                                                                                " Carousel sliders"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-image-box-content.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-image"
                                                                                }),
                                                                                " Image box content"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-tabs.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-tab-window"
                                                                                }),
                                                                                " Tabs"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-counters.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-timer"
                                                                                }),
                                                                                " Counters"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-shop-widgets.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-shopping-cart"
                                                                                }),
                                                                                " Shop Widgets"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-filters.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-check-box"
                                                                                }),
                                                                                " Gallery Filters"
                                                                            ]
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Element"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-accordians.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-accordion-list"
                                                                                }),
                                                                                " Accordians"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-dividers.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-list-post"
                                                                                }),
                                                                                " Dividers"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-images-effects.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-media-overlay"
                                                                                }),
                                                                                " Images effects"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-testimonials.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-comments"
                                                                                }),
                                                                                " Testimonials"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-pagination.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-more"
                                                                                }),
                                                                                " Pagination"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-alert-box.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-alert"
                                                                                }),
                                                                                " Alert box"
                                                                            ]
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                        children: [
                                                            " ",
                                                            /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                href: "#!",
                                                                children: "Element"
                                                            }),
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-icon-box.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-media-left-alt"
                                                                                }),
                                                                                " Icon Box"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-list-group.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-list"
                                                                                }),
                                                                                " List group"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-title-separators.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-line-solid"
                                                                                }),
                                                                                " Title Separators"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-all-widgets.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-widgetized"
                                                                                }),
                                                                                " Widgets"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-carousel-sliders.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-layout-slider"
                                                                                }),
                                                                                " Carousel sliders"
                                                                            ]
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                                            href: "shortcode-image-box-content.html",
                                                                            children: [
                                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                                    className: "ti-image"
                                                                                }),
                                                                                " Image box content"
                                                                            ]
                                                                        })
                                                                    })
                                                                ]
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                        children: [
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("a", {
                                                href: "#!",
                                                children: [
                                                    "Contact Us",
                                                    /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                        className: "fa fa-chevron-down"
                                                    })
                                                ]
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                className: "sub-menu right",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "contact-1.html",
                                                            children: "Contact us 1"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "contact-2.html",
                                                            children: "Contact us 2"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "contact-3.html",
                                                            children: "Contact us 3"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                            href: "contact-4.html",
                                                            children: "Contact us 4"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                className: "dlab-social-icon",
                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "site-button circle fa fa-facebook",
                                                href: "#!"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "site-button circle fa fa-twitter",
                                                href: "#!"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "site-button circle fa fa-linkedin",
                                                href: "#!"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                className: "site-button circle fa fa-instagram",
                                                href: "#!"
                                            })
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Header/Header.jsx




function Header() {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("header", {
        className: "site-header mo-left header",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(TopBar, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(NavBar, {
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Footer/Top/Top.jsx


function Top() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "footer-top",
        style: {
            backgroundImage: 'url(images/pattern/pt15.png)'
        },
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-xl-3 col-lg-3 col-md-6 col-5 col-sm-6 footer-col-4 col-12",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "widget widget_about border-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "footer-title",
                                    children: "About Us"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: "mm-t5",
                                    children: "Contrary to popular belief, Lorem simply random text. It has roots in a piece of classical Latin literature."
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                    className: "contact-info-bx",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                    className: "las la-map-marker"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                    children: "Address"
                                                }),
                                                " 20 , New York 10010 "
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                    className: "las la-phone-volume"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                    children: "Phone"
                                                }),
                                                " 0800-123456"
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                    className: "las la-envelope-open"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                    children: "Email"
                                                }),
                                                " info@example.com"
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-xl-3 col-lg-4 col-md-6 col-7 col-sm-6 footer-col-4 col-12",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "widget border-0 recent-posts-entry",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "footer-title",
                                    children: "Latest Post"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "widget-post-bx",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "widget-post clearfix",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "dlab-post-media",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "/images/blog/recent-blog/pic1.jpg",
                                                        width: "200",
                                                        height: "143",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "dlab-post-info",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "dlab-post-header",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                                className: "post-title",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                    href: "blog-single.html",
                                                                    children: "Helping you and your house become better."
                                                                })
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "dlab-post-meta",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                                    className: "post-date",
                                                                    children: [
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "la la-clock"
                                                                        }),
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                            children: "01 June"
                                                                        }),
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: " 2020"
                                                                        }),
                                                                        " "
                                                                    ]
                                                                })
                                                            })
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                            className: "widget-post clearfix",
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                    className: "dlab-post-media",
                                                    children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "/images/blog/recent-blog/pic2.jpg",
                                                        width: "200",
                                                        height: "160",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                    className: "dlab-post-info",
                                                    children: [
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "dlab-post-header",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                                className: "post-title",
                                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                                    href: "blog-single.html",
                                                                    children: "Creating quality urban lifestyles."
                                                                })
                                                            })
                                                        }),
                                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                            className: "dlab-post-meta",
                                                            children: /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                                    className: "post-date",
                                                                    children: [
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "la la-clock"
                                                                        }),
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("strong", {
                                                                            children: "01 June"
                                                                        }),
                                                                        " ",
                                                                        /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                            children: " 2020"
                                                                        }),
                                                                        " "
                                                                    ]
                                                                })
                                                            })
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-xl-3 col-lg-2 col-md-6 col-sm-6 footer-col-4 col-12",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "widget widget_services border-0",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "footer-title",
                                    children: "Usefull Link"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                    className: "mm-t10",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                href: "#!",
                                                children: "About Us"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                href: "#!",
                                                children: "Blog"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                href: "#!",
                                                children: "Services"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                href: "#!",
                                                children: "Privacy Policy"
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                href: "#!",
                                                children: "Projects "
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-xl-3 col-lg-3 col-md-6 col-sm-6 footer-col-4 col-12",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "widget",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h5", {
                                    className: "footer-title",
                                    children: "Opening Hours"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                    className: "thsn-timelist-list mm-t5",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-title",
                                                    children: "Mon – Tue"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-value",
                                                    children: "10:00 – 18:00"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-title",
                                                    children: "Wed – Thur"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-value",
                                                    children: "10:00 – 17:00"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-title",
                                                    children: "Fri – Sat"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-value",
                                                    children: "10:00 – 12:30"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-title",
                                                    children: "Saturday"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-value",
                                                    children: "10:00 – 12:30"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-title",
                                                    children: "Sunday"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "thsn-timelist-li-value",
                                                    children: "Closed"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Footer/Bottom/Bottom.jsx


function Bottom() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "footer-bottom",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-6 col-sm-6 text-left ",
                        children: [
                            " ",
                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                children: "Copyright \xa9 2020 DexignZone"
                            }),
                            " "
                        ]
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "col-md-6 col-sm-6 text-right ",
                        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "widget-link ",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            href: "#!",
                                            children: " About"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            href: "#!",
                                            children: " Help Desk"
                                        })
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                        children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                            href: "#!",
                                            children: " Privacy Policy"
                                        })
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Footer/Footer.jsx




function Footer() {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("footer", {
        className: "site-footer",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Top, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Bottom, {
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Contact/Contact.jsx


function Contact() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "section-full p-tb50 bg-primary text-white",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: "container",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "row align-items-center",
                children: [
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-7 m-md-b30",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                children: "Subscribe To Our Newsletter"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                className: "m-b0",
                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words"
                            })
                        ]
                    }),
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "col-md-5",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("h4", {
                                children: "Your Email Address"
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("form", {
                                className: "dzSubscribe style1",
                                action: "script/mailchamp.php",
                                method: "post",
                                children: [
                                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                        className: "dzSubscribeMsg"
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "input-group",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                                name: "dzEmail",
                                                required: "required",
                                                type: "email",
                                                className: "form-control",
                                                placeholder: "Your Email Address"
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "input-group-addon",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                    name: "submit",
                                                    value: "Submit",
                                                    type: "submit",
                                                    className: "site-button-secondry btnhover13",
                                                    children: "Subscribe"
                                                })
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};
;

;// CONCATENATED MODULE: ./components/Layout/components/Content/Slider/Slider.jsx


function Slider() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "rev-slider",
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            id: "rev_slider_1164_1_wrapper",
            className: "rev_slider_wrapper fullscreen-container",
            "data-alias": "exploration-header",
            "data-source": "gallery",
            style: {
                backgroundColor: 'transparent',
                padding: '0px'
            },
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                id: "rev_slider_1164_1",
                className: "rev_slider fullscreenbanner",
                style: {
                    display: 'none'
                },
                "data-version": "5.4.1",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                            "data-index": "rs-3204",
                            "data-transition": "slideoververtical",
                            "data-slotamount": "default",
                            "data-hideafterloop": "0",
                            "data-hideslideonmobile": "off",
                            "data-easein": "default",
                            "data-easeout": "default",
                            "data-masterspeed": "default",
                            "data-thumb": "images/main-slider/slide8.png",
                            "data-rotate": "0",
                            "data-fstransition": "fade",
                            "data-fsmasterspeed": "2000",
                            "data-fsslotamount": "7",
                            "data-saveperformance": "off",
                            "data-title": "",
                            "data-param1": "What our team has found in the wild",
                            "data-param2": "",
                            "data-param3": "",
                            "data-param4": "",
                            "data-param5": "",
                            "data-param6": "",
                            "data-param7": "",
                            "data-param8": "",
                            "data-param9": "",
                            "data-param10": "",
                            "data-description": "",
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                    src: "images/main-slider/slide8.png",
                                    alt: "",
                                    "data-lazyload": "images/main-slider/slide8.png",
                                    "data-bgposition": "center center",
                                    "data-bgfit": "cover",
                                    "data-bgrepeat": "no-repeat",
                                    "data-bgparallax": "6",
                                    className: "rev-slidebg",
                                    "data-no-retina": true
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "tp-caption tp-shape tp-shapewrapper",
                                    id: "slide-101-layer-14",
                                    "data-x": "['center','center','center','center']",
                                    "data-hoffset": "['0','0','0','0']",
                                    "data-y": "['middle','middle','middle','middle']",
                                    "data-voffset": "['0','0','0','0']",
                                    "data-width": "full",
                                    "data-height": "full",
                                    "data-whitespace": "nowrap",
                                    "data-type": "shape",
                                    "data-basealign": "slide",
                                    "data-responsive_offset": "off",
                                    "data-responsive": "off",
                                    "data-frames": "[{\"delay\":10,\"speed\":1000,\"frame\":\"0\",\"from\":\"opacity:0;\",\"to\":\"o:1;\",\"ease\":\"Power3.easeInOut\"},{\"delay\":\"wait\",\"speed\":1500,\"frame\":\"999\",\"to\":\"opacity:0;\",\"ease\":\"Power4.easeIn\"}]",
                                    "data-textalign": "['inherit','inherit','inherit','inherit']",
                                    "data-paddingtop": "[0,0,0,0]",
                                    "data-paddingright": "[0,0,0,0]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[0,0,0,0]",
                                    style: {
                                        zIndex: '5',
                                        fontFamily: 'Open Sans',
                                        backgroundColor: 'rgba(0,0,0,0.15)',
                                        backgroundSize: '100%',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundPosition: 'bottom'
                                    }
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "tp-caption",
                                    id: "slide-3204-layer-2",
                                    "data-x": "['center','center','center','center']",
                                    "data-hoffset": "['-210','-180','0','0']",
                                    "data-y": "['middle','middle','middle','middle']",
                                    "data-voffset": "['-80','-5','-180','-150']",
                                    "data-width": "['700','600','600','260']",
                                    "data-fontsize": "['60','45','30','24']",
                                    "data-lineheight": "['70','45','36','30']",
                                    "data-height": "none",
                                    "data-whitespace": "normal",
                                    "data-type": "text",
                                    "data-basealign": "slide",
                                    "data-responsive_offset": "off",
                                    "data-responsive": "off",
                                    "data-textalign": "['left','left','center','center']",
                                    "data-frames": "[{\"from\":\"y:50px;opacity:0;\",\"speed\":1500,\"to\":\"o:1;\",\"delay\":650,\"ease\":\"Power4.easeOut\"},{\"delay\":\"wait\",\"speed\":300,\"to\":\"opacity:0;\",\"ease\":\"nothing\"}]",
                                    "data-paddingtop": "[0,0,0,0]",
                                    "data-paddingright": "[0,0,0,0]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[0,0,0,0]",
                                    style: {
                                        zIndex: '7',
                                        whiteSpace: 'normal',
                                        color: '#fff',
                                        fontFamily: 'Poppins, serif',
                                        borderWidth: '0px',
                                        fontWeight: '600',
                                        fontFamily: 'Merriweather, serif'
                                    },
                                    children: [
                                        "Get Your Business To ",
                                        /*#__PURE__*/ jsx_runtime_.jsx("br", {
                                        }),
                                        "Scale Up Quickly"
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "tp-caption tp-resizeme rs-parallaxlevel-1",
                                    id: "slide-100-layer-5",
                                    "data-x": "['right','right','middle','middle']",
                                    "data-hoffset": "['-330','-400','0','0']",
                                    "data-y": "['bottom','bottom','bottom','bottom']",
                                    "data-voffset": "['-40','-40','-20','-100']",
                                    "data-width": "none",
                                    "data-height": "none",
                                    "data-whitespace": "nowrap",
                                    "data-type": "image",
                                    "data-responsive_offset": "on",
                                    "data-frames": "[{\"delay\":250,\"speed\":5000,\"frame\":\"0\",\"from\":\"y:50px;rZ:5deg;opacity:0;fb:50px;\",\"to\":\"o:1;fb:0;\",\"ease\":\"Power4.easeOut\"},{\"delay\":\"wait\",\"speed\":300,\"frame\":\"999\",\"to\":\"opacity:0;fb:0;\",\"ease\":\"Power3.easeInOut\"}]",
                                    "data-textalign": "['inherit','inherit','inherit','inherit']",
                                    "data-paddingtop": "[0,0,0,0]",
                                    "data-paddingright": "[0,0,0,0]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[0,0,0,0]",
                                    style: {
                                        zIndex: '11'
                                    }
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "tp-caption ",
                                    id: "slide-3204-layer-3",
                                    "data-x": "['center','center','center','center']",
                                    "data-hoffset": "['-255','-230','0','0']",
                                    "data-y": "['middle','middle','middle','middle']",
                                    "data-voffset": "['50','40','-100','-60']",
                                    "data-width": "['600','500','500','350']",
                                    "data-fontsize": "['18','16','14','14']",
                                    "data-lineheight": "['30','26','24','24']",
                                    "data-height": "none",
                                    "data-whitespace": "normal",
                                    "data-type": "text",
                                    "data-basealign": "slide",
                                    "data-responsive_offset": "off",
                                    "data-responsive": "off",
                                    "data-frames": "[{\"from\":\"y:50px;opacity:0;\",\"speed\":2000,\"to\":\"o:1;\",\"delay\":750,\"ease\":\"Power4.easeOut\"},{\"delay\":\"wait\",\"speed\":300,\"to\":\"opacity:0;\",\"ease\":\"nothing\"}]",
                                    "data-textalign": "['left','left','center','center']",
                                    "data-paddingtop": "[0,100,0,0]",
                                    "data-paddingright": "[0,0,0,0]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[0,0,0,0]",
                                    style: {
                                        zIndex: '7',
                                        whiteSpace: 'normal',
                                        color: '#fff',
                                        fontFamily: 'Montserrat, sans-serif',
                                        borderWidth: '0px',
                                        fontWeight: '400'
                                    },
                                    children: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the."
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "tp-caption rev-btn bg-primary btnhover13 tp-resizeme",
                                    href: "about-1.html",
                                    target: "_blank",
                                    id: "slide-411-layer-13",
                                    "data-x": "['center','center','center','center']",
                                    "data-hoffset": "['-470','-400','-90','-70']",
                                    "data-y": "['center','center','middle','middle']",
                                    "data-voffset": "['150','180','-20','40']",
                                    "data-width": "none",
                                    "data-height": "none",
                                    "data-whitespace": "['nowrap','nowrap','nowrap','nowrap']",
                                    "data-type": "button",
                                    "data-actions": "",
                                    "data-basealign": "slide",
                                    "data-responsive_offset": "off",
                                    "data-responsive": "off",
                                    "data-frames": "[{\"delay\":\"+690\",\"speed\":2000,\"frame\":\"0\",\"from\":\"y:50px;opacity:0;fb:20px;\",\"to\":\"o:1;fb:0;\",\"ease\":\"Power4.easeOut\"},{\"delay\":\"wait\",\"speed\":300,\"frame\":\"999\",\"to\":\"opacity:0;fb:0;\",\"ease\":\"Power3.easeInOut\"},{\"frame\":\"hover\",\"speed\":\"0\",\"ease\":\"Linear.easeNone\",\"to\":\"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;\",\"style\":\"c:rgba(255, 255, 255, 1.00);bg:var(--color-hover);\"}]",
                                    "data-margintop": "[0,0,0,0]",
                                    "data-marginright": "[0,0,0,0]",
                                    "data-marginbottom": "[0,0,0,0]",
                                    "data-marginleft": "[0,0,0,0]",
                                    "data-textalign": "['inherit','inherit','inherit','inherit']",
                                    "data-paddingtop": "[0,0,0,0]",
                                    "data-paddingright": "[35,35,35,20]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[35,35,35,20]",
                                    style: {
                                        zIndex: '13',
                                        whiteSpace: 'normal',
                                        fontSize: '17px',
                                        lineHeight: '50px',
                                        fontWeight: '500',
                                        color: 'rgba(255, 255, 255, 1.00)',
                                        display: 'inline-block',
                                        fontFamily: 'Poppins',
                                        borderColor: 'rgba(255, 255, 255, 1.00)',
                                        borderStyle: 'solid',
                                        borderWidth: '0',
                                        borderRadius: '4px',
                                        outline: 'none',
                                        boxShadow: 'none',
                                        boxSizing: 'border-box',
                                        MozBoxSizing: 'border-box',
                                        WebkitBoxSizing: 'border-box',
                                        cursor: 'pointer',
                                        textDecoration: 'none'
                                    },
                                    children: "Read More"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                    className: "tp-caption rev-btn tp-resizeme btnhover13",
                                    href: "about-1.html",
                                    target: "_blank",
                                    id: "slide-411-layer-12",
                                    "data-x": "['center','center','center','center']",
                                    "data-hoffset": "['-290','-230','90','70']",
                                    "data-y": "['center','center','middle','middle']",
                                    "data-voffset": "['150','180','-20','40']",
                                    "data-width": "none",
                                    "data-height": "none",
                                    "data-whitespace": "['nowrap','nowrap','nowrap','nowrap']",
                                    "data-type": "button",
                                    "data-actions": "",
                                    "data-basealign": "slide",
                                    "data-responsive_offset": "off",
                                    "data-responsive": "off",
                                    "data-frames": "[{\"delay\":\"+690\",\"speed\":2000,\"frame\":\"0\",\"from\":\"y:50px;opacity:0;fb:20px;\",\"to\":\"o:1;fb:0;\",\"ease\":\"Power4.easeOut\"},{\"delay\":\"wait\",\"speed\":300,\"frame\":\"999\",\"to\":\"opacity:0;fb:0;\",\"ease\":\"Power3.easeInOut\"},{\"frame\":\"hover\",\"speed\":\"0\",\"ease\":\"Linear.easeNone\",\"to\":\"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;\",\"style\":\"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);\"}]",
                                    "data-margintop": "[0,0,0,0]",
                                    "data-marginright": "[0,0,0,0]",
                                    "data-marginbottom": "[0,0,0,0]",
                                    "data-marginleft": "[0,0,0,0]",
                                    "data-textalign": "['inherit','inherit','inherit','inherit']",
                                    "data-paddingtop": "[0,0,0,0]",
                                    "data-paddingright": "[34,34,34,19]",
                                    "data-paddingbottom": "[0,0,0,0]",
                                    "data-paddingleft": "[34,34,34,19]",
                                    style: {
                                        zIndex: '13',
                                        whiteSpace: 'normal',
                                        fontSize: '17px',
                                        lineHeight: '50px',
                                        fontWeight: '500',
                                        color: 'rgba(255, 255, 255, 1.00)',
                                        display: 'inline-block',
                                        fontFamily: 'Poppins',
                                        backgroundColor: 'rgba(255, 255, 255, 0)',
                                        borderColor: 'rgba(255, 255, 255, 1.00)',
                                        borderStyle: 'solid',
                                        borderWidth: '1px 1px 1px 1px',
                                        borderRadius: '4px',
                                        outline: 'none',
                                        boxShadow: 'none',
                                        boxSizing: 'border-box',
                                        MozBoxSizing: 'border-box',
                                        WebkitBoxSizing: 'border-box',
                                        cursor: 'pointer',
                                        textDecoration: 'none'
                                    },
                                    children: "About Us"
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "tp-bannertimer tp-bottom",
                        style: {
                            visibility: 'hidden !important'
                        }
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Project.jsx


function Project() {
    return(/*#__PURE__*/ jsx_runtime_.jsx("div", {
        className: "section-full bg-gray content-inner about-carousel-ser",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "container",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "section-head text-center",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                            className: "title",
                            children: "POPULAR COURSES"
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("p", {
                            children: "There are many variations of passages of Lorem Ipsum typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer."
                        })
                    ]
                }),
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-md-6 col-lg-3 col-sm-6",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "dlab-box courses-bx",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-media",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                src: "images/our-services/pic1.jpg",
                                                alt: ""
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "user-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "images/testimonials/pic1.jpg",
                                                        alt: ""
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                        className: "title",
                                                        children: "Jack Ronan"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "review",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "item-review",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-half-o"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-o"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "10 Review"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: "courses-details.html",
                                                    children: "Learn Python – Interactive Python Tutorial"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Lorem Ipsum is simply dummy text of the printing and typesetting."
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "courses-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                    className: "fa fa-users"
                                                                }),
                                                                " 20 Student "
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "price",
                                                        children: "$79.00"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-md-6 col-lg-3 col-sm-6",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "dlab-box courses-bx",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-media",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                src: "images/our-services/pic2.jpg",
                                                alt: ""
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "user-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "images/testimonials/pic2.jpg",
                                                        alt: ""
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                        className: "title",
                                                        children: "Jack Ronan"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "review",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "item-review",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-half-o"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-o"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "10 Review"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: "courses-details.html",
                                                    children: "Learn Python – Interactive Python Tutorial"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Lorem Ipsum is simply dummy text of the printing and typesetting."
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "courses-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                    className: "fa fa-users"
                                                                }),
                                                                " 20 Student "
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "price",
                                                        children: "$79.00"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-md-6 col-lg-3 col-sm-6",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "dlab-box courses-bx",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-media",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                src: "images/our-services/pic3.jpg",
                                                alt: ""
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "user-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "images/testimonials/pic3.jpg",
                                                        alt: ""
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                        className: "title",
                                                        children: "Jack Ronan"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "review",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "item-review",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-half-o"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-o"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "10 Review"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: "courses-details.html",
                                                    children: "Learn Python – Interactive Python Tutorial"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Lorem Ipsum is simply dummy text of the printing and typesetting."
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "courses-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                    className: "fa fa-users"
                                                                }),
                                                                " 20 Student "
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "price",
                                                        children: "$79.00"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: "col-md-6 col-lg-3 col-sm-6",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "dlab-box courses-bx",
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-media",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                src: "images/our-services/pic4.jpg",
                                                alt: ""
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "user-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                        src: "images/testimonials/pic2.jpg",
                                                        alt: ""
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                        className: "title",
                                                        children: "Jack Ronan"
                                                    }),
                                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "review",
                                                        children: [
                                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("ul", {
                                                                className: "item-review",
                                                                children: [
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-half-o"
                                                                        })
                                                                    }),
                                                                    /*#__PURE__*/ jsx_runtime_.jsx("li", {
                                                                        children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                            className: "fa fa-star-o"
                                                                        })
                                                                    })
                                                                ]
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                children: "10 Review"
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("h6", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    href: "courses-details.html",
                                                    children: "Learn Python – Interactive Python Tutorial"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                children: "Lorem Ipsum is simply dummy text of the printing and typesetting."
                                            }),
                                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "courses-info",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("li", {
                                                            children: [
                                                                /*#__PURE__*/ jsx_runtime_.jsx("i", {
                                                                    className: "fa fa-users"
                                                                }),
                                                                " 20 Student "
                                                            ]
                                                        })
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                        className: "price",
                                                        children: "$79.00"
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Discount.jsx


function Discount() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full bg-img-fix content-inner-2 overlay-black-dark contact-action style2",
        style: {
            backgroundImage: 'url(images/background/bg2.jpg)'
        },
        children: /*#__PURE__*/ _jsx("div", {
            className: "container",
            children: /*#__PURE__*/ _jsxs("div", {
                className: "row relative",
                children: [
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-12 col-lg-8 wow fadeInLeft",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.2s",
                        children: /*#__PURE__*/ _jsxs("div", {
                            className: "contact-no-area",
                            children: [
                                /*#__PURE__*/ _jsx("h2", {
                                    className: "title",
                                    children: "Create your free account now and get immediate access to 100s of online courses."
                                }),
                                /*#__PURE__*/ _jsxs("form", {
                                    action: "/script/mailchamp.php",
                                    method: "post",
                                    className: "dzSubscribe subscribe-box row align-items-center sp15",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "col-lg-12",
                                            children: /*#__PURE__*/ _jsx("div", {
                                                className: "dzSubscribeMsg"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "col-lg-4 col-md-4",
                                            children: /*#__PURE__*/ _jsx("div", {
                                                className: "input-group",
                                                children: /*#__PURE__*/ _jsx("input", {
                                                    name: "dzName",
                                                    required: "required",
                                                    type: "text",
                                                    className: "form-control",
                                                    placeholder: "Your Name "
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "col-lg-4 col-md-4",
                                            children: /*#__PURE__*/ _jsx("div", {
                                                className: "input-group",
                                                children: /*#__PURE__*/ _jsx("input", {
                                                    name: "dzEmail",
                                                    required: "required",
                                                    type: "email",
                                                    className: "form-control",
                                                    placeholder: "Your Email Address"
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "col-lg-4 col-md-4",
                                            children: /*#__PURE__*/ _jsx("div", {
                                                className: "input-group",
                                                children: /*#__PURE__*/ _jsx("button", {
                                                    name: "submit",
                                                    value: "Submit",
                                                    type: "submit",
                                                    className: "site-button btn-block btnhover13",
                                                    children: "Subscribe"
                                                })
                                            })
                                        })
                                    ]
                                })
                            ]
                        })
                    }),
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-12 col-lg-4 contact-img-bx wow fadeInRight",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.2s",
                        children: /*#__PURE__*/ _jsx("img", {
                            src: "/images/pic1.png",
                            alt: ""
                        })
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Service.jsx


function Service() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full bg-white content-inner",
        children: /*#__PURE__*/ _jsx("div", {
            className: "container",
            children: /*#__PURE__*/ _jsxs("div", {
                className: "row service-area-one",
                children: [
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-lg-4 m-b30 hidden-sm",
                        children: /*#__PURE__*/ _jsx("div", {
                            className: "rdx-thu",
                            children: /*#__PURE__*/ _jsx("img", {
                                src: "/images/student1.png",
                                alt: ""
                            })
                        })
                    }),
                    /*#__PURE__*/ _jsxs("div", {
                        className: "col-lg-8",
                        children: [
                            /*#__PURE__*/ _jsxs("div", {
                                className: "section-head",
                                children: [
                                    /*#__PURE__*/ _jsx("h2", {
                                        className: "title",
                                        children: " Welcome To HubTechoo"
                                    }),
                                    /*#__PURE__*/ _jsx("p", {
                                        children: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                                    })
                                ]
                            }),
                            /*#__PURE__*/ _jsxs("div", {
                                className: "row",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-secondry radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-paint-brush"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "Special Education"
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-secondry radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-calendar"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "Events"
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-primary radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-book"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "Full Day Session"
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-primary radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-graduation-cap"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "Pre Classes "
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-secondry radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-user"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "Qualified Teachers"
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    }),
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "col-md-6 col-sm-6 m-b30",
                                        children: /*#__PURE__*/ _jsxs("div", {
                                            className: "icon-bx-wraper left serb",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "icon-bx-xs bg-secondry radius",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "#",
                                                        className: "icon-cell",
                                                        children: /*#__PURE__*/ _jsx("i", {
                                                            className: "fa fa-clock-o"
                                                        })
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsxs("div", {
                                                    className: "icon-content",
                                                    children: [
                                                        /*#__PURE__*/ _jsx("h5", {
                                                            className: "rdx-tilte",
                                                            children: "24/7 Supports"
                                                        }),
                                                        /*#__PURE__*/ _jsx("p", {
                                                            children: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.."
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Counter.jsx


function Counter() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full text-white bg-img-fix content-inner overlay-black-dark counter-staus-box",
        style: {
            backgroundImage: 'url(images/background/bg4.jpg'
        },
        children: /*#__PURE__*/ _jsx("div", {
            className: "container",
            children: /*#__PURE__*/ _jsxs("div", {
                className: "row",
                children: [
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.2s",
                        children: /*#__PURE__*/ _jsx("div", {
                            className: "icon-bx-wraper center",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "icon-content",
                                children: [
                                    /*#__PURE__*/ _jsx("h2", {
                                        className: "dlab-tilte counter",
                                        children: "1226"
                                    }),
                                    /*#__PURE__*/ _jsx("p", {
                                        children: "Happy Students"
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.4s",
                        children: /*#__PURE__*/ _jsx("div", {
                            className: "icon-bx-wraper center",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "icon-content",
                                children: [
                                    /*#__PURE__*/ _jsx("h2", {
                                        className: "dlab-tilte counter",
                                        children: "1552"
                                    }),
                                    /*#__PURE__*/ _jsx("p", {
                                        children: "Approved Courses"
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.6s",
                        children: /*#__PURE__*/ _jsx("div", {
                            className: "icon-bx-wraper center",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "icon-content",
                                children: [
                                    /*#__PURE__*/ _jsx("h2", {
                                        className: "dlab-tilte counter",
                                        children: "1156"
                                    }),
                                    /*#__PURE__*/ _jsx("p", {
                                        children: "Certified Teachers"
                                    })
                                ]
                            })
                        })
                    }),
                    /*#__PURE__*/ _jsx("div", {
                        className: "col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5",
                        "data-wow-duration": "2s",
                        "data-wow-delay": "0.6s",
                        children: /*#__PURE__*/ _jsx("div", {
                            className: "icon-bx-wraper center",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "icon-content",
                                children: [
                                    /*#__PURE__*/ _jsx("h2", {
                                        className: "dlab-tilte counter",
                                        children: "2100"
                                    }),
                                    /*#__PURE__*/ _jsx("p", {
                                        children: "Graduate Students"
                                    })
                                ]
                            })
                        })
                    })
                ]
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Team.jsx


function Team() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full bg-gray content-inner",
        children: /*#__PURE__*/ _jsxs("div", {
            className: "container",
            children: [
                /*#__PURE__*/ _jsxs("div", {
                    className: "section-head text-center ",
                    children: [
                        /*#__PURE__*/ _jsx("h2", {
                            className: "title",
                            children: " Meet The Teacher"
                        }),
                        /*#__PURE__*/ _jsx("p", {
                            children: "There are many variations of passages of Lorem Ipsum typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer."
                        })
                    ]
                }),
                /*#__PURE__*/ _jsxs("div", {
                    className: "row",
                    children: [
                        /*#__PURE__*/ _jsx("div", {
                            className: "col-lg-3 col-md-6 col-sm-6 wow fadeInUp",
                            "data-wow-duration": "2s",
                            "data-wow-delay": "0.2s",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "dlab-box m-b30 dlab-team1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-media",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "teachers-profile.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                width: "358",
                                                height: "460",
                                                alt: "",
                                                src: "images/our-team/preview.jpg",
                                                className: "lazy",
                                                "data-src": "images/our-team/pic17.jpg"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ _jsx("h4", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ _jsx("a", {
                                                    href: "teachers-profile.html",
                                                    children: "Nashid Martines"
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("span", {
                                                className: "dlab-position",
                                                children: "Director"
                                            }),
                                            /*#__PURE__*/ _jsxs("ul", {
                                                className: "dlab-social-icon dez-border",
                                                children: [
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-facebook",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-twitter",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-linkedin",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-pinterest",
                                                            href: "javascript:void(0);"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "col-lg-3 col-md-6 col-sm-6 wow fadeInUp",
                            "data-wow-duration": "2s",
                            "data-wow-delay": "0.4s",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "dlab-box m-b30 dlab-team1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-media",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "teachers-profile.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                width: "358",
                                                height: "460",
                                                alt: "",
                                                src: "images/our-team/preview.jpg",
                                                className: "lazy",
                                                "data-src": "images/our-team/pic16.jpg"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ _jsx("h4", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ _jsx("a", {
                                                    href: "teachers-profile.html",
                                                    children: "Konne Backfield"
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("span", {
                                                className: "dlab-position",
                                                children: "Designer"
                                            }),
                                            /*#__PURE__*/ _jsxs("ul", {
                                                className: "dlab-social-icon dez-border",
                                                children: [
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-facebook",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-twitter",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-linkedin",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-pinterest",
                                                            href: "javascript:void(0);"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "col-lg-3 col-md-6 col-sm-6 wow fadeInUp",
                            "data-wow-duration": "2s",
                            "data-wow-delay": "0.6s",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "dlab-box m-b30 dlab-team1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-media",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "teachers-profile.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                width: "358",
                                                height: "460",
                                                alt: "",
                                                src: "images/our-team/preview.jpg",
                                                className: "lazy",
                                                "data-src": "images/our-team/pic15.jpg"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ _jsx("h4", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ _jsx("a", {
                                                    href: "teachers-profile.html",
                                                    children: "Hackson Willingham"
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("span", {
                                                className: "dlab-position",
                                                children: "Developer"
                                            }),
                                            /*#__PURE__*/ _jsxs("ul", {
                                                className: "dlab-social-icon dez-border",
                                                children: [
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-facebook",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-twitter",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-linkedin",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-pinterest",
                                                            href: "javascript:void(0);"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "col-lg-3 col-md-6 col-sm-6 wow fadeInUp",
                            "data-wow-duration": "2s",
                            "data-wow-delay": "0.8s",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "dlab-box m-b30 dlab-team1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-media",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "teachers-profile.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                width: "358",
                                                height: "460",
                                                alt: "",
                                                src: "images/our-team/preview.jpg",
                                                className: "lazy",
                                                "data-src": "images/our-team/pic18.jpg"
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info",
                                        children: [
                                            /*#__PURE__*/ _jsx("h4", {
                                                className: "dlab-title",
                                                children: /*#__PURE__*/ _jsx("a", {
                                                    href: "teachers-profile.html",
                                                    children: "Konne Backfield"
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("span", {
                                                className: "dlab-position",
                                                children: "Manager"
                                            }),
                                            /*#__PURE__*/ _jsxs("ul", {
                                                className: "dlab-social-icon dez-border",
                                                children: [
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-facebook",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-twitter",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-linkedin",
                                                            href: "javascript:void(0);"
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsx("li", {
                                                        children: /*#__PURE__*/ _jsx("a", {
                                                            className: "fa fa-pinterest",
                                                            href: "javascript:void(0);"
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Comment.jsx


function Comment() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full overlay-black-middle bg-secondry content-inner-2 wow fadeIn",
        "data-wow-duration": "2s",
        "data-wow-delay": "0.2s",
        style: {
            backgroundImage: 'url(images/background/map-bg.png'
        },
        children: /*#__PURE__*/ _jsxs("div", {
            className: "container",
            children: [
                /*#__PURE__*/ _jsxs("div", {
                    className: "section-head text-white text-center",
                    children: [
                        /*#__PURE__*/ _jsx("h2", {
                            className: "title",
                            children: "What People Are Saying"
                        }),
                        /*#__PURE__*/ _jsx("p", {
                            children: "There are many variations of passages of Lorem Ipsum typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer."
                        })
                    ]
                }),
                /*#__PURE__*/ _jsx("div", {
                    className: "section-content",
                    children: /*#__PURE__*/ _jsxs("div", {
                        className: "testimonial-two-dots owl-carousel owl-none owl-theme owl-dots-primary-full owl-loaded owl-drag",
                        children: [
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text ",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic3.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic2.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic1.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text ",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic3.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic2.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            }),
                            /*#__PURE__*/ _jsx("div", {
                                className: "item",
                                children: /*#__PURE__*/ _jsxs("div", {
                                    className: "testimonial-15 quote-right",
                                    children: [
                                        /*#__PURE__*/ _jsx("div", {
                                            className: "testimonial-text",
                                            children: /*#__PURE__*/ _jsx("p", {
                                                children: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum"
                                            })
                                        }),
                                        /*#__PURE__*/ _jsxs("div", {
                                            className: "testimonial-detail clearfix",
                                            children: [
                                                /*#__PURE__*/ _jsx("div", {
                                                    className: "testimonial-pic radius",
                                                    children: /*#__PURE__*/ _jsx("img", {
                                                        src: "images/testimonials/pic1.jpg",
                                                        width: "100",
                                                        height: "100",
                                                        alt: ""
                                                    })
                                                }),
                                                /*#__PURE__*/ _jsx("strong", {
                                                    className: "testimonial-name",
                                                    children: "David Matin"
                                                }),
                                                " ",
                                                /*#__PURE__*/ _jsx("span", {
                                                    className: "testimonial-position",
                                                    children: "Student"
                                                })
                                            ]
                                        })
                                    ]
                                })
                            })
                        ]
                    })
                })
            ]
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Blog.jsx


function Blog() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full content-inner bg-gray wow fadeIn",
        "data-wow-duration": "2s",
        "data-wow-delay": "0.4s",
        children: /*#__PURE__*/ _jsxs("div", {
            className: "container",
            children: [
                /*#__PURE__*/ _jsxs("div", {
                    className: "section-head text-center",
                    children: [
                        /*#__PURE__*/ _jsx("h2", {
                            className: "title",
                            children: "Latest blog post"
                        }),
                        /*#__PURE__*/ _jsx("p", {
                            children: "There are many variations of passages of Lorem Ipsum typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer."
                        })
                    ]
                }),
                /*#__PURE__*/ _jsxs("div", {
                    className: "blog-carousel owl-none owl-carousel",
                    children: [
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "blog-post blog-grid blog-rounded blog-effect1 post-style-1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-post-media dlab-img-effect",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "blog-single.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                src: "/images/blog/grid/pic2.jpg",
                                                alt: ""
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info p-a20",
                                        children: [
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-meta",
                                                children: /*#__PURE__*/ _jsxs("ul", {
                                                    children: [
                                                        /*#__PURE__*/ _jsxs("li", {
                                                            className: "post-author",
                                                            children: [
                                                                /*#__PURE__*/ _jsx("i", {
                                                                    className: "la la-user-circle"
                                                                }),
                                                                " By ",
                                                                /*#__PURE__*/ _jsx("a", {
                                                                    href: "javascript:void(0);",
                                                                    children: "Reuben"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ _jsx("li", {
                                                            className: "post-tag",
                                                            children: /*#__PURE__*/ _jsx("a", {
                                                                href: "javascript:void(0);",
                                                                children: "Event"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-title",
                                                children: /*#__PURE__*/ _jsx("h4", {
                                                    className: "post-title",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "blog-single.html",
                                                        children: "The Shocking Revelation of Education."
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-text",
                                                children: /*#__PURE__*/ _jsx("p", {
                                                    children: "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary."
                                                })
                                            }),
                                            /*#__PURE__*/ _jsxs("div", {
                                                className: "post-footer",
                                                children: [
                                                    /*#__PURE__*/ _jsx("div", {
                                                        className: "dlab-post-meta",
                                                        children: /*#__PURE__*/ _jsx("ul", {
                                                            children: /*#__PURE__*/ _jsxs("li", {
                                                                className: "post-date",
                                                                children: [
                                                                    /*#__PURE__*/ _jsx("i", {
                                                                        className: "la la-clock"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("strong", {
                                                                        children: "01 June"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("span", {
                                                                        children: " 2020"
                                                                    })
                                                                ]
                                                            })
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsxs("ul", {
                                                        className: "dlab-social-icon dez-border",
                                                        children: [
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button facebook circle-sm fa fa-facebook",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button twitter circle-sm fa fa-twitter ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button linkedin circle-sm fa fa-linkedin ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button instagram circle-sm fa fa-instagram ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "blog-post blog-grid blog-rounded blog-effect1 post-style-1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-post-media dlab-img-effect",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "blog-single.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                src: "/images/blog/grid/pic3.jpg",
                                                alt: ""
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info p-a20",
                                        children: [
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-meta",
                                                children: /*#__PURE__*/ _jsxs("ul", {
                                                    children: [
                                                        /*#__PURE__*/ _jsxs("li", {
                                                            className: "post-author",
                                                            children: [
                                                                /*#__PURE__*/ _jsx("i", {
                                                                    className: "la la-user-circle"
                                                                }),
                                                                " By ",
                                                                /*#__PURE__*/ _jsx("a", {
                                                                    href: "javascript:void(0);",
                                                                    children: "Caroline"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ _jsx("li", {
                                                            className: "post-tag",
                                                            children: /*#__PURE__*/ _jsx("a", {
                                                                href: "javascript:void(0);",
                                                                children: "Education"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-title ",
                                                children: /*#__PURE__*/ _jsx("h4", {
                                                    className: "post-title",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "blog-single.html",
                                                        children: "Five Things Nobody Told You About"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-text",
                                                children: /*#__PURE__*/ _jsx("p", {
                                                    children: "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary."
                                                })
                                            }),
                                            /*#__PURE__*/ _jsxs("div", {
                                                className: "post-footer",
                                                children: [
                                                    /*#__PURE__*/ _jsx("div", {
                                                        className: "dlab-post-meta",
                                                        children: /*#__PURE__*/ _jsx("ul", {
                                                            children: /*#__PURE__*/ _jsxs("li", {
                                                                className: "post-date",
                                                                children: [
                                                                    /*#__PURE__*/ _jsx("i", {
                                                                        className: "la la-clock"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("strong", {
                                                                        children: "01 June"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("span", {
                                                                        children: " 2020"
                                                                    })
                                                                ]
                                                            })
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsxs("ul", {
                                                        className: "dlab-social-icon dez-border",
                                                        children: [
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button facebook circle-sm fa fa-facebook",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button twitter circle-sm fa fa-twitter ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button linkedin circle-sm fa fa-linkedin ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button instagram circle-sm fa fa-instagram ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsxs("div", {
                                className: "blog-post blog-grid blog-rounded blog-effect1 post-style-1",
                                children: [
                                    /*#__PURE__*/ _jsx("div", {
                                        className: "dlab-post-media dlab-img-effect",
                                        children: /*#__PURE__*/ _jsx("a", {
                                            href: "blog-single.html",
                                            children: /*#__PURE__*/ _jsx("img", {
                                                src: "/images/blog/grid/pic4.jpg",
                                                alt: ""
                                            })
                                        })
                                    }),
                                    /*#__PURE__*/ _jsxs("div", {
                                        className: "dlab-info p-a20",
                                        children: [
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-meta",
                                                children: /*#__PURE__*/ _jsxs("ul", {
                                                    children: [
                                                        /*#__PURE__*/ _jsxs("li", {
                                                            className: "post-author",
                                                            children: [
                                                                /*#__PURE__*/ _jsx("i", {
                                                                    className: "la la-user-circle"
                                                                }),
                                                                " By ",
                                                                /*#__PURE__*/ _jsx("a", {
                                                                    href: "javascript:void(0);",
                                                                    children: "Harry"
                                                                })
                                                            ]
                                                        }),
                                                        /*#__PURE__*/ _jsx("li", {
                                                            className: "post-tag",
                                                            children: /*#__PURE__*/ _jsx("a", {
                                                                href: "javascript:void(0);",
                                                                children: "knowledge"
                                                            })
                                                        })
                                                    ]
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-title ",
                                                children: /*#__PURE__*/ _jsx("h4", {
                                                    className: "post-title",
                                                    children: /*#__PURE__*/ _jsx("a", {
                                                        href: "blog-single.html",
                                                        children: "Here's What People Are Saying About"
                                                    })
                                                })
                                            }),
                                            /*#__PURE__*/ _jsx("div", {
                                                className: "dlab-post-text",
                                                children: /*#__PURE__*/ _jsx("p", {
                                                    children: "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary."
                                                })
                                            }),
                                            /*#__PURE__*/ _jsxs("div", {
                                                className: "post-footer",
                                                children: [
                                                    /*#__PURE__*/ _jsx("div", {
                                                        className: "dlab-post-meta",
                                                        children: /*#__PURE__*/ _jsx("ul", {
                                                            children: /*#__PURE__*/ _jsxs("li", {
                                                                className: "post-date",
                                                                children: [
                                                                    /*#__PURE__*/ _jsx("i", {
                                                                        className: "la la-clock"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("strong", {
                                                                        children: "01 June"
                                                                    }),
                                                                    " ",
                                                                    /*#__PURE__*/ _jsx("span", {
                                                                        children: " 2020"
                                                                    })
                                                                ]
                                                            })
                                                        })
                                                    }),
                                                    /*#__PURE__*/ _jsxs("ul", {
                                                        className: "dlab-social-icon dez-border",
                                                        children: [
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button facebook circle-sm fa fa-facebook",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button twitter circle-sm fa fa-twitter ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button linkedin circle-sm fa fa-linkedin ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            }),
                                                            /*#__PURE__*/ _jsx("li", {
                                                                children: /*#__PURE__*/ _jsx("a", {
                                                                    className: "site-button instagram circle-sm fa fa-instagram ",
                                                                    href: "javascript:void(0);"
                                                                })
                                                            })
                                                        ]
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            ]
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Module/Client.jsx


function Client() {
    return(/*#__PURE__*/ _jsx("div", {
        className: "section-full dlab-we-find bg-img-fix p-t20 p-b20 bg-white wow fadeIn",
        "data-wow-duration": "2s",
        "data-wow-delay": "0.6s",
        children: /*#__PURE__*/ _jsx("div", {
            className: "container",
            children: /*#__PURE__*/ _jsx("div", {
                className: "section-content",
                children: /*#__PURE__*/ _jsxs("div", {
                    className: "client-logo-carousel mfp-gallery gallery owl-btn-center-lr owl-carousel owl-btn-3",
                    children: [
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo1.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo2.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo1.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo3.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo4.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        }),
                        /*#__PURE__*/ _jsx("div", {
                            className: "item",
                            children: /*#__PURE__*/ _jsx("div", {
                                className: "ow-client-logo",
                                children: /*#__PURE__*/ _jsx("div", {
                                    className: "client-logo",
                                    children: /*#__PURE__*/ _jsx("a", {
                                        href: "javascript:void(0);",
                                        children: /*#__PURE__*/ _jsx("img", {
                                            src: "images/client-logo/logo3.jpg",
                                            alt: ""
                                        })
                                    })
                                })
                            })
                        })
                    ]
                })
            })
        })
    }));
};

;// CONCATENATED MODULE: ./components/Layout/components/Content/Content.jsx











function Content() {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "page-content bg-white",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Slider, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "content-block",
                children: /*#__PURE__*/ jsx_runtime_.jsx(Project, {
                })
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./components/Layout/Layout.jsx






function Layout() {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        className: "page-wraper",
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx(Header, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Content, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Contact, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(Footer, {
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                className: "scroltop icon-up",
                type: "button",
                children: /*#__PURE__*/ jsx_runtime_.jsx("i", {
                    className: "fa fa-arrow-up"
                })
            })
        ]
    }));
};

;// CONCATENATED MODULE: ./pages/_app.js



function MyApp({ Component , pageProps  }) {
    return(/*#__PURE__*/ jsx_runtime_.jsx(Layout, {
        children: /*#__PURE__*/ jsx_runtime_.jsx(Component, {
            ...pageProps
        })
    }));
}
/* harmony default export */ const _app = (MyApp);


/***/ }),

/***/ 689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(642));
module.exports = __webpack_exports__;

})();