import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
            <meta charSet="utf-8"/>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="keywords" content="" />
            <meta name="author" content="" />
            <meta name="robots" content="" />
            <meta name="description" content="EduZone - Education Collage  School HTML5 Template" />
            <meta property="og:title" content="EduZone - Education Collage School HTML5 Template" />
            <meta property="og:description" content="EduZone - Education Collage School HTML5 Template" />
            <meta property="og:image" content="" />
            <meta name="format-detection" content="telephone=no"/>  

            
            <link rel="icon" href="/images/favicon.ico" type="image/x-icon" />
            <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png" />

            
            <title>Get Your Business To Scale Up Quickly.</title>

            
            <meta name="viewport" content="width=device-width, initial-scale=1"/>

            
            <script src="/js/html5shiv.min.js"></script>
            <script src="/js/respond.min.js"></script>
                
            <link rel="stylesheet" type="text/css" href="/css/plugins.css"/>
            <link rel="stylesheet" type="text/css" href="/css/style.css"/>
            <link class="skin" rel="stylesheet" type="text/css" href="/css/skin/skin-1.css"/>
            <link rel="stylesheet" href="/css/templete.css"/>
            
            <link
              href="https://fonts.googleapis.com/css2?family=Merriweather"
              rel="stylesheet"
            />
            
            <link rel="stylesheet" type="text/css" href="/plugins/revolution/revolution/css/revolution.min.css"/>
        </Head>
        <body id="bg">
          <Main></Main>
          <NextScript>
            <script src="/js/jquery.min.js"></script>
            <script src="/plugins/owl-carousel/owl.carousel.js"></script>
            <script src="/js/dz.carousel.js"></script>
            <script src="/plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
          </NextScript>
        </body>
      </Html>
    )
  }
}

export default MyDocument