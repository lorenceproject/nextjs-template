import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default function TopBar() {
    return (
        <div className="top-bar">
            <div className="container">
                    <div className="row d-flex justify-content-between align-items-center">
                        <div className="dlab-topbar-left">
                            <ul>
                                <li><i className="la la-phone-volume"></i> +00 888 6668811</li>
                                <li><i className="las la-map-marker"></i> 1073 W Stephen Ave, Clawson</li>
                            </ul>
                        </div>
                        <div className="dlab-topbar-right">
                            <ul>
                                <li><i className="la la-clock"></i>  Mon - Sat 8.00 - 18.00</li>
                                <li><i className="las la-envelope-open"></i> info@example.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    );
}