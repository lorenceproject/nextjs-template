import React, { Component } from 'react';
import TopBar from './TopBar/TopBar';
import NavBar from './NavBar/NavBar';

export default function Header() {
    return (
        <header className="site-header mo-left header">
            <TopBar/>
            <NavBar/>
        </header>
    );
}