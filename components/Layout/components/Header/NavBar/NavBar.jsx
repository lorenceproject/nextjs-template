import React, { Component } from 'react';

export default function NavBar() {
    return (
        <div className="sticky-header main-bar-wraper navbar-expand-lg">
            <div className="main-bar clearfix ">
                <div className="container clearfix">                   
                    <div className="logo-header mostion logo-dark">
                        <a href="index.html"><img src="images/logo.png" alt=""/></a>
                    </div>
                    <button className="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <div className="extra-nav">
                        <div className="extra-cell">
                            <button id="quik-search-btn" type="button" className="site-button-link"><i className="la la-search"></i></button>
                            <a href="#!" className="site-button btnhover13">Apply Now</a>
                        </div>
                    </div>
                    <div className="dlab-quik-search ">
                        <form action="#">
                            <input name="search" defaultValue="" type="text" className="form-control" placeholder="Type to search"/>
                            <span id="quik-search-remove"><i className="ti-close"></i></span>
                        </form>
                    </div>
                    <div className="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
                        <div className="logo-header d-md-block d-lg-none">
                            <a href="index.html"><img src="images/logo.png" alt=""/></a>
                        </div>
                        <ul className="nav navbar-nav">
                            <li className="active">
                                <a href="#!">Home<i className="fa fa-chevron-down"></i></a>
                                <ul className="sub-menu right">
                                    <li><a href="index.html">Home - University</a></li>
                                    <li><a href="index-2.html">Home - Kindergarten</a></li>
                                    <li><a href="index-3.html">Home - Collage</a></li>
                                    <li><a href="index-4.html">Home - Coaching</a></li>
                                    <li><a href="index-5.html">Home - School</a></li>
                                    <li><a href="index-6.html">Home - Online Courese</a></li>
                                    <li><a href="index-7.html">Home - Language School</a></li>
                                    <li><a href="index-8.html">Home - Kids School</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#!">Features<i className="fa fa-chevron-down"></i></a>
                                <ul className="sub-menu tab-content">
                                    <li>
                                        <a href="#!">Header Light <i className="fa fa-angle-right"></i></a>
                                        <ul className="sub-menu">
                                            <li><a href="header-style-1.html">Header 1</a></li>
                                            <li><a href="header-style-2.html">Header 2</a></li>
                                            <li><a href="header-style-3.html">Header 3</a></li>
                                            <li><a href="header-style-4.html">Header 4</a></li>
                                            <li><a href="header-style-5.html">Header 5</a></li>
                                            <li><a href="header-style-6.html">Header 6</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Header Dark <i className="fa fa-angle-right"></i></a>
                                        <ul className="sub-menu">
                                            <li><a href="header-style-dark-1.html">Header 1</a></li>
                                            <li><a href="header-style-dark-2.html">Header 2</a></li>
                                            <li><a href="header-style-dark-3.html">Header 3</a></li>
                                            <li><a href="header-style-dark-4.html">Header 4</a></li>
                                            <li><a href="header-style-dark-5.html">Header 5</a></li>
                                            <li><a href="header-style-dark-6.html">Header 6</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Footer <i className="fa fa-angle-right"></i></a>
                                        <ul className="sub-menu">
                                            <li><a href="footer-1.html">Footer 1 </a></li>
                                            <li><a href="footer-2.html">Footer 2</a></li>
                                            <li><a href="footer-3.html">Footer 3</a></li>
                                            <li><a href="footer-4.html">Footer 4</a></li>
                                            <li><a href="footer-5.html">Footer 5</a></li>
                                            <li><a href="footer-6.html">Footer 6</a></li>
                                            <li><a href="footer-7.html">Footer 7</a></li>
                                            <li><a href="footer-8.html">Footer 8</a></li>
                                            <li><a href="footer-9.html">Footer 9</a></li>
                                            <li><a href="footer-10.html">Footer 10</a></li>
                                            <li><a href="footer-11.html">Footer 11</a></li>
                                            <li><a href="footer-12.html">Footer 12</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li className="has-mega-menu"> <a href="#!">Pages<i className="fa fa-chevron-down"></i></a>
                                <ul className="mega-menu">
                                    <li>
                                        <a href="#!">Pages</a>
                                        <ul>
                                            <li><a href="about-1.html">About us 1</a></li>
                                            <li><a href="about-2.html">About us 2</a></li>
                                            <li><a href="services-1.html">Services 1</a></li>
                                            <li><a href="services-2.html">Services 2</a></li>
                                            <li><a href="faq-1.html">Faqs </a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Pages</a>
                                        <ul>
                                            <li><a href="teacher.html">Teachers</a></li>
                                            <li><a href="teachers-profile.html">Teachers Profile</a></li>
                                            <li><a href="courses.html">Courses</a></li>
                                            <li><a href="courses-details.html">Courses Details</a></li>
                                            <li><a href="event.html">Events</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Pages</a>
                                        <ul>
                                            <li><a href="event-details.html">Events Details</a></li>
                                            <li><a href="help-desk.html">Help Desk</a></li>
                                            <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                            <li><a href="error-404.html">Error 404</a></li>
                                            <li><a href="error-405.html">Error 405</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#!">Pages</a>
                                        <ul>
                                            <li><a href="gallery-grid-2.html">Gallery Grid 2</a></li>
                                            <li><a href="gallery-grid-3.html">Gallery Grid 3</a></li>
                                            <li><a href="gallery-grid-4.html">Gallery Grid 4</a></li>
                                            <li><a href="coming-soon-1.html">Coming Soon 1</a></li>
                                            <li><a href="coming-soon-2.html">Coming Soon 2</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#!">Shop<i className="fa fa-chevron-down"></i></a>
                                <ul className="sub-menu">
                                    <li><a href="shop.html">Shop</a></li>
                                    <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
                                    <li><a href="shop-product-details.html">Product Details</a></li>
                                    <li><a href="shop-cart.html">Cart</a></li>
                                    <li><a href="shop-wishlist.html">Wishlist</a></li>
                                    <li><a href="shop-checkout.html">Checkout</a></li>
                                    <li><a href="shop-login.html">Login</a></li>
                                    <li><a href="shop-register.html">Register</a></li>
                                </ul>
                            </li>
                            <li className="has-mega-menu"> <a href="#!">Blog<i className="fa fa-chevron-down"></i></a>
                                <ul className="mega-menu">
                                    <li> <a href="#!">Blog</a>
                                        <ul>
                                            <li><a href="blog-half-img.html">Half image</a></li>
                                            <li><a href="blog-half-img-sidebar.html">Half image sidebar</a></li>
                                            <li><a href="blog-half-img-left-sidebar.html">Half image sidebar left</a></li>
                                            <li><a href="blog-large-img.html">Large image</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Blog</a>
                                        <ul>
                                            <li><a href="blog-large-img-sidebar.html">Large image sidebar</a></li>
                                            <li><a href="blog-large-img-left-sidebar.html">Large image sidebar left</a></li>
                                            <li><a href="blog-grid-2.html">Grid 2</a></li>
                                            <li><a href="blog-grid-2-sidebar.html">Grid 2 sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Blog</a>
                                        <ul>
                                            <li><a href="blog-grid-2-sidebar-left.html">Grid 2 sidebar left</a></li>
                                            <li><a href="blog-grid-3.html">Grid 3</a></li>
                                            <li><a href="blog-grid-3-sidebar.html">Grid 3 sidebar</a></li>
                                            <li><a href="blog-grid-3-sidebar-left.html">Grid 3 sidebar left</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Blog</a>
                                        <ul>
                                            <li><a href="blog-grid-4.html">Grid 4</a></li>
                                            <li><a href="blog-single.html">Single</a></li>
                                            <li><a href="blog-single-sidebar.html">Single sidebar</a></li>
                                            <li><a href="blog-single-left-sidebar.html">Single sidebar right</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li className="has-mega-menu"> <a href="#!">Element<i className="fa fa-chevron-down"></i></a>
                                <ul className="mega-menu">
                                    <li><a href="#!">Element</a>
                                        <ul>
                                            <li><a href="shortcode-buttons.html"><i className="ti-mouse"></i> Buttons</a></li>
                                            <li><a href="shortcode-icon-box-styles.html"><i className="ti-layout-grid2"></i> Icon box styles</a></li>
                                            <li><a href="shortcode-pricing-table.html"><i className="ti-layout-grid2-thumb"></i> Pricing table</a></li>
                                            <li><a href="shortcode-toggles.html"><i className="ti-layout-accordion-separated"></i> Toggles</a></li>
                                            <li><a href="shortcode-team.html"><i className="ti-user"></i> Team</a></li>
                                            <li><a href="shortcode-animation-effects.html"><i className="ti-layers-alt"></i> Animation Effects</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Element</a>
                                        <ul>
                                            <li><a href="shortcode-carousel-sliders.html"><i className="ti-layout-slider"></i> Carousel sliders</a></li>
                                            <li><a href="shortcode-image-box-content.html"><i className="ti-image"></i> Image box content</a></li>
                                            <li><a href="shortcode-tabs.html"><i className="ti-layout-tab-window"></i> Tabs</a></li>
                                            <li><a href="shortcode-counters.html"><i className="ti-timer"></i> Counters</a></li>
                                            <li><a href="shortcode-shop-widgets.html"><i className="ti-shopping-cart"></i> Shop Widgets</a></li>
                                            <li><a href="shortcode-filters.html"><i className="ti-check-box"></i> Gallery Filters</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Element</a>
                                        <ul>
                                            <li><a href="shortcode-accordians.html"><i className="ti-layout-accordion-list"></i> Accordians</a></li>
                                            <li><a href="shortcode-dividers.html"><i className="ti-layout-list-post"></i> Dividers</a></li>
                                            <li><a href="shortcode-images-effects.html"><i className="ti-layout-media-overlay"></i> Images effects</a></li>
                                            <li><a href="shortcode-testimonials.html"><i className="ti-comments"></i> Testimonials</a></li>
                                            <li><a href="shortcode-pagination.html"><i className="ti-more"></i> Pagination</a></li>
                                            <li><a href="shortcode-alert-box.html"><i className="ti-alert"></i> Alert box</a></li>
                                        </ul>
                                    </li>
                                    <li> <a href="#!">Element</a>
                                        <ul>
                                            <li><a href="shortcode-icon-box.html"><i className="ti-layout-media-left-alt"></i> Icon Box</a></li>
                                            <li><a href="shortcode-list-group.html"><i className="ti-list"></i> List group</a></li>
                                            <li><a href="shortcode-title-separators.html"><i className="ti-layout-line-solid"></i> Title Separators</a></li>
                                            <li><a href="shortcode-all-widgets.html"><i className="ti-widgetized"></i> Widgets</a></li>
                                            <li><a href="shortcode-carousel-sliders.html"><i className="ti-layout-slider"></i> Carousel sliders</a></li>
                                            <li><a href="shortcode-image-box-content.html"><i className="ti-image"></i> Image box content</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#!">Contact Us<i className="fa fa-chevron-down"></i></a>
                                <ul className="sub-menu right">
                                    <li><a href="contact-1.html">Contact us 1</a></li>
                                    <li><a href="contact-2.html">Contact us 2</a></li>
                                    <li><a href="contact-3.html">Contact us 3</a></li>
                                    <li><a href="contact-4.html">Contact us 4</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div className="dlab-social-icon">
                            <ul>
                                <li><a className="site-button circle fa fa-facebook" href="#!"></a></li>
                                <li><a className="site-button  circle fa fa-twitter" href="#!"></a></li>
                                <li><a className="site-button circle fa fa-linkedin" href="#!"></a></li>
                                <li><a className="site-button circle fa fa-instagram" href="#!"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}