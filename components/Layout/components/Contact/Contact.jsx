import React, { Component } from 'react';

export default function Contact() {
    return (
        <div className="section-full p-tb50 bg-primary text-white">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-md-7 m-md-b30">
                        <h4>Subscribe To Our Newsletter</h4>
                        <p className="m-b0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words</p>
                    </div>
                    <div className="col-md-5">
                        <h4>Your Email Address</h4>
                        <form className="dzSubscribe style1" action="script/mailchamp.php" method="post">
                            <div className="dzSubscribeMsg"></div>
                            <div className="input-group">
                                <input name="dzEmail" required="required" type="email" className="form-control" placeholder="Your Email Address"/>
                                <div className="input-group-addon">
                                    <button name="submit" value="Submit" type="submit" className="site-button-secondry btnhover13">Subscribe</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
};