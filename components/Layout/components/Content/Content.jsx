import React, { Component } from 'react';
import Slider from './Slider/Slider';
import Project from './Module/Project';
import Discount from './Module/Discount';
import Service from './Module/Service';
import Counter from './Module/Counter';
import Team from './Module/Team';
import Comment from './Module/Comment';
import Blog from './Module/Blog';
import Client from './Module/Client';

export default function Content() {
    return (
        <div className="page-content bg-white">                
            <Slider/>
            <div className="content-block">
                <Project/>
            </div>
        </div>
    );
}