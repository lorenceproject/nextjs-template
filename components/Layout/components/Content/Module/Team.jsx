import React, { Component } from 'react';

export default function Team() {
    return (
        <div className="section-full bg-gray content-inner">
            <div className="container">
                <div className="section-head text-center ">
                    <h2 className="title"> Meet The Teacher</h2>
                    <p>There are many variations of passages of Lorem Ipsum typesetting industry has been the
                        industry's standard dummy text ever since the been when an unknown printer.</p>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="2s"
                            data-wow-delay="0.2s">
                        <div className="dlab-box m-b30 dlab-team1">
                            <div className="dlab-media">
                                <a href="teachers-profile.html">
                                    <img width="358" height="460" alt="" src="images/our-team/preview.jpg"
                                            className="lazy" data-src="images/our-team/pic17.jpg"/>
                                </a>
                            </div>
                            <div className="dlab-info">
                                <h4 className="dlab-title"><a href="teachers-profile.html">Nashid Martines</a></h4>
                                <span className="dlab-position">Director</span>
                                <ul className="dlab-social-icon dez-border">
                                    <li><a className="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-pinterest" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="2s"
                            data-wow-delay="0.4s">
                        <div className="dlab-box m-b30 dlab-team1">
                            <div className="dlab-media">
                                <a href="teachers-profile.html">
                                    <img width="358" height="460" alt="" src="images/our-team/preview.jpg"
                                            className="lazy" data-src="images/our-team/pic16.jpg"/>
                                </a>
                            </div>
                            <div className="dlab-info">
                                <h4 className="dlab-title"><a href="teachers-profile.html">Konne Backfield</a></h4>
                                <span className="dlab-position">Designer</span>
                                <ul className="dlab-social-icon dez-border">
                                    <li><a className="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-pinterest" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="2s"
                            data-wow-delay="0.6s">
                        <div className="dlab-box m-b30 dlab-team1">
                            <div className="dlab-media">
                                <a href="teachers-profile.html">
                                    <img width="358" height="460" alt="" src="images/our-team/preview.jpg"
                                            className="lazy" data-src="images/our-team/pic15.jpg"/>
                                </a>
                            </div>
                            <div className="dlab-info">
                                <h4 className="dlab-title"><a href="teachers-profile.html">Hackson Willingham</a>
                                </h4>
                                <span className="dlab-position">Developer</span>
                                <ul className="dlab-social-icon dez-border">
                                    <li><a className="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-pinterest" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6 wow fadeInUp" data-wow-duration="2s"
                            data-wow-delay="0.8s">
                        <div className="dlab-box m-b30 dlab-team1">
                            <div className="dlab-media">
                                <a href="teachers-profile.html">
                                    <img width="358" height="460" alt="" src="images/our-team/preview.jpg"
                                            className="lazy" data-src="images/our-team/pic18.jpg"/>
                                </a>
                            </div>
                            <div className="dlab-info">
                                <h4 className="dlab-title"><a href="teachers-profile.html">Konne Backfield</a></h4>
                                <span className="dlab-position">Manager</span>
                                <ul className="dlab-social-icon dez-border">
                                    <li><a className="fa fa-facebook" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-twitter" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-linkedin" href="javascript:void(0);"></a></li>
                                    <li><a className="fa fa-pinterest" href="javascript:void(0);"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}