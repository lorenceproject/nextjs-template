import React, { Component } from 'react';

export default function Counter() {
    return (
        <div className="section-full text-white bg-img-fix content-inner overlay-black-dark counter-staus-box"
                style={{backgroundImage: 'url(images/background/bg4.jpg'}}>
            <div className="container">
                <div className="row">
                    <div className="col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5"
                            data-wow-duration="2s" data-wow-delay="0.2s">
                        <div className="icon-bx-wraper center">
                            <div className="icon-content">
                                <h2 className="dlab-tilte counter">1226</h2>
                                <p>Happy Students</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5"
                            data-wow-duration="2s" data-wow-delay="0.4s">
                        <div className="icon-bx-wraper center">
                            <div className="icon-content">
                                <h2 className="dlab-tilte counter">1552</h2>
                                <p>Approved Courses</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5"
                            data-wow-duration="2s" data-wow-delay="0.6s">
                        <div className="icon-bx-wraper center">
                            <div className="icon-content">
                                <h2 className="dlab-tilte counter">1156</h2>
                                <p>Certified Teachers</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-lg-3 col-sm-6 col-6 m-b30 wow fadeInUp counter-style-5"
                            data-wow-duration="2s" data-wow-delay="0.6s">
                        <div className="icon-bx-wraper center">
                            <div className="icon-content">
                                <h2 className="dlab-tilte counter">2100</h2>
                                <p>Graduate Students</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}