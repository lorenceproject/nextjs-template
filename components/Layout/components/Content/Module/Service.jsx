import React, { Component } from 'react';

export default function Service() {
    return (
        <div className="section-full bg-white content-inner">
            <div className="container">
                <div className="row service-area-one">
                    <div className="col-lg-4 m-b30 hidden-sm">
                        <div className="rdx-thu"><img src="/images/student1.png" alt=""/></div>
                    </div>
                    <div className="col-lg-8">
                        <div className="section-head">
                            <h2 className="title"> Welcome To HubTechoo</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown printer took a galley of type and scrambled it to make a type specimen
                                book.</p>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left">
                                    <div className="icon-bx-xs bg-secondry radius">
                                        <a href="#" className="icon-cell">
                                            <i className="fa fa-paint-brush"/></a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">Special Education</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left">
                                    <div className="icon-bx-xs bg-secondry radius">
                                        <a href="#" className="icon-cell">
                                            <i className="fa fa-calendar"/>
                                        </a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">Events</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left">
                                    <div className="icon-bx-xs bg-primary radius"><a href="#" className="icon-cell">
                                        <i className="fa fa-book"/></a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">Full Day Session</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left">
                                    <div className="icon-bx-xs bg-primary radius">
                                        <a href="#" className="icon-cell"><i className="fa fa-graduation-cap"/></a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">Pre Classes </h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left">
                                    <div className="icon-bx-xs bg-secondry radius">
                                        <a href="#" className="icon-cell">
                                            <i className="fa fa-user"/>
                                        </a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">Qualified Teachers</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-6 m-b30">
                                <div className="icon-bx-wraper left serb">
                                    <div className="icon-bx-xs bg-secondry radius">
                                        <a href="#" className="icon-cell">
                                            <i className="fa fa-clock-o"/>
                                        </a>
                                    </div>
                                    <div className="icon-content">
                                        <h5 className="rdx-tilte">24/7 Supports</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}