import React, { Component } from 'react';

export default function Discount() {
    return (
        <div className="section-full bg-img-fix content-inner-2 overlay-black-dark contact-action style2"
                style={{backgroundImage: 'url(images/background/bg2.jpg)'}}>
            <div className="container">
                <div className="row relative">
                    <div className="col-md-12 col-lg-8 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
                        <div className="contact-no-area">
                            <h2 className="title">Create your free account now and get immediate access to 100s of
                                online courses.</h2>
                            <form action="/script/mailchamp.php" method="post"
                                    className="dzSubscribe subscribe-box row align-items-center sp15">
                                <div className="col-lg-12">
                                    <div className="dzSubscribeMsg"></div>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <input name="dzName" required="required" type="text"
                                                className="form-control" placeholder="Your Name "/>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <input name="dzEmail" required="required" type="email"
                                                className="form-control" placeholder="Your Email Address"/>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-md-4">
                                    <div className="input-group">
                                        <button name="submit" value="Submit" type="submit"
                                                className="site-button btn-block btnhover13">Subscribe
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-md-12 col-lg-4 contact-img-bx wow fadeInRight" data-wow-duration="2s"
                            data-wow-delay="0.2s">
                        <img src="/images/pic1.png" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    );
}