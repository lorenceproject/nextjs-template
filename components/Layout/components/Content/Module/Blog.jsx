import React, { Component } from 'react';

export default function Blog() {
    return (
        <div className="section-full content-inner bg-gray wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
            <div className="container">
                <div className="section-head text-center">
                    <h2 className="title">Latest blog post</h2>
                    <p>There are many variations of passages of Lorem Ipsum typesetting industry has been the
                        industry's standard dummy text ever since the been when an unknown printer.</p>
                </div>
                <div className="blog-carousel owl-none owl-carousel">
                    <div className="item">
                        <div className="blog-post blog-grid blog-rounded blog-effect1 post-style-1">
                            <div className="dlab-post-media dlab-img-effect">
                                <a href="blog-single.html"><img src="/images/blog/grid/pic2.jpg" alt=""/></a>
                            </div>
                            <div className="dlab-info p-a20">
                                <div className="dlab-post-meta">
                                    <ul>
                                        <li className="post-author">
                                            <i className="la la-user-circle"/> By <a
                                            href="javascript:void(0);">Reuben</a></li>
                                        <li className="post-tag"><a href="javascript:void(0);">Event</a></li>
                                    </ul>
                                </div>
                                <div className="dlab-post-title">
                                    <h4 className="post-title"><a href="blog-single.html">The Shocking Revelation of
                                        Education.</a></h4>
                                </div>
                                <div className="dlab-post-text">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined
                                        chunks as necessary.</p>
                                </div>
                                <div className="post-footer">
                                    <div className="dlab-post-meta">
                                        <ul>
                                            <li className="post-date"><i className="la la-clock"/> <strong>01
                                                June</strong> <span> 2020</span></li>
                                        </ul>
                                    </div>
                                    <ul className="dlab-social-icon dez-border">
                                        <li><a className="site-button facebook circle-sm fa fa-facebook"
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button twitter circle-sm fa fa-twitter "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button linkedin circle-sm fa fa-linkedin "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button instagram circle-sm fa fa-instagram  "
                                                href="javascript:void(0);"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="blog-post blog-grid blog-rounded blog-effect1 post-style-1">
                            <div className="dlab-post-media dlab-img-effect">
                                <a href="blog-single.html"><img src="/images/blog/grid/pic3.jpg" alt=""/></a>
                            </div>
                            <div className="dlab-info p-a20">
                                <div className="dlab-post-meta">
                                    <ul>
                                        <li className="post-author"><i className="la la-user-circle"></i> By <a
                                            href="javascript:void(0);">Caroline</a></li>
                                        <li className="post-tag"><a href="javascript:void(0);">Education</a></li>
                                    </ul>
                                </div>
                                <div className="dlab-post-title ">
                                    <h4 className="post-title"><a href="blog-single.html">Five Things Nobody Told
                                        You About</a></h4>
                                </div>
                                <div className="dlab-post-text">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined
                                        chunks as necessary.</p>
                                </div>
                                <div className="post-footer">
                                    <div className="dlab-post-meta">
                                        <ul>
                                            <li className="post-date"><i className="la la-clock"></i> <strong>01
                                                June</strong> <span> 2020</span></li>
                                        </ul>
                                    </div>
                                    <ul className="dlab-social-icon dez-border">
                                        <li><a className="site-button facebook circle-sm fa fa-facebook"
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button twitter circle-sm fa fa-twitter "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button linkedin circle-sm fa fa-linkedin "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button instagram circle-sm fa fa-instagram  "
                                                href="javascript:void(0);"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="blog-post blog-grid blog-rounded blog-effect1 post-style-1">
                            <div className="dlab-post-media dlab-img-effect">
                                <a href="blog-single.html"><img src="/images/blog/grid/pic4.jpg" alt=""/></a>
                            </div>
                            <div className="dlab-info p-a20">
                                <div className="dlab-post-meta">
                                    <ul>
                                        <li className="post-author"><i className="la la-user-circle"/> By <a
                                            href="javascript:void(0);">Harry</a></li>
                                        <li className="post-tag"><a href="javascript:void(0);">knowledge</a></li>
                                    </ul>
                                </div>
                                <div className="dlab-post-title ">
                                    <h4 className="post-title"><a href="blog-single.html">Here's What People Are
                                        Saying About</a></h4>
                                </div>
                                <div className="dlab-post-text">
                                    <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined
                                        chunks as necessary.</p>
                                </div>
                                <div className="post-footer">
                                    <div className="dlab-post-meta">
                                        <ul>
                                            <li className="post-date"><i className="la la-clock"/> <strong>01
                                                June</strong> <span> 2020</span></li>
                                        </ul>
                                    </div>
                                    <ul className="dlab-social-icon dez-border">
                                        <li><a className="site-button facebook circle-sm fa fa-facebook"
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button twitter circle-sm fa fa-twitter "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button linkedin circle-sm fa fa-linkedin "
                                                href="javascript:void(0);"/></li>
                                        <li><a className="site-button instagram circle-sm fa fa-instagram  "
                                                href="javascript:void(0);"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}