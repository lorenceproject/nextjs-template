import React, { Component } from 'react';

export default function Project() {
    return (
        <div className="section-full bg-gray content-inner about-carousel-ser">
            <div className="container">
                <div className="section-head text-center">
                    <h2 className="title">POPULAR COURSES</h2>
                    <p>There are many variations of passages of Lorem Ipsum typesetting industry has been the industry's standard dummy text ever since the been when an unknown printer.</p>
                </div>
                <div className="row">
                    <div className="col-md-6 col-lg-3 col-sm-6">
                        <div className="dlab-box courses-bx">
                            <div className="dlab-media">
                                <img src="images/our-services/pic1.jpg" alt=""/>
                                <div className="user-info">
                                    <img src="images/testimonials/pic1.jpg" alt=""/>
                                    <h6 className="title">Jack Ronan</h6>
                                    <div className="review">
                                        <ul className="item-review">
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star-half-o"></i></li>
                                            <li><i className="fa fa-star-o"></i></li>
                                        </ul>
                                        <span>10 Review</span>
                                    </div>
                                </div>
                            </div>
                            <div className="dlab-info">
                                <h6 className="dlab-title"><a href="courses-details.html">Learn Python – Interactive Python Tutorial</a></h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                <div className="courses-info">
                                    <ul>
                                        <li><i className="fa fa-users"></i> 20 Student </li>
                                    </ul>
                                    <span className="price">$79.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3 col-sm-6">
                        <div className="dlab-box courses-bx">
                            <div className="dlab-media">
                                <img src="images/our-services/pic2.jpg" alt=""/>
                                <div className="user-info">
                                    <img src="images/testimonials/pic2.jpg" alt=""/>
                                    <h6 className="title">Jack Ronan</h6>
                                    <div className="review">
                                        <ul className="item-review">
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star-half-o"></i></li>
                                            <li><i className="fa fa-star-o"></i></li>
                                        </ul>
                                        <span>10 Review</span>
                                    </div>
                                </div>
                            </div>
                            <div className="dlab-info">
                                <h6 className="dlab-title"><a href="courses-details.html">Learn Python – Interactive Python Tutorial</a></h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                <div className="courses-info">
                                    <ul>
                                        <li><i className="fa fa-users"></i> 20 Student </li>
                                    </ul>
                                    <span className="price">$79.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3 col-sm-6">
                        <div className="dlab-box courses-bx">
                            <div className="dlab-media">
                                <img src="images/our-services/pic3.jpg" alt=""/>
                                <div className="user-info">
                                    <img src="images/testimonials/pic3.jpg" alt=""/>
                                    <h6 className="title">Jack Ronan</h6>
                                    <div className="review">
                                        <ul className="item-review">
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star-half-o"></i></li>
                                            <li><i className="fa fa-star-o"></i></li>
                                        </ul>
                                        <span>10 Review</span>
                                    </div>
                                </div>
                            </div>
                            <div className="dlab-info">
                                <h6 className="dlab-title"><a href="courses-details.html">Learn Python – Interactive Python Tutorial</a></h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                <div className="courses-info">
                                    <ul>
                                        <li><i className="fa fa-users"></i> 20 Student </li>
                                    </ul>
                                    <span className="price">$79.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-lg-3 col-sm-6">
                        <div className="dlab-box courses-bx">
                            <div className="dlab-media">
                                <img src="images/our-services/pic4.jpg" alt=""/>
                                <div className="user-info">
                                    <img src="images/testimonials/pic2.jpg" alt=""/>
                                    <h6 className="title">Jack Ronan</h6>
                                    <div className="review">
                                        <ul className="item-review">
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star"></i></li>
                                            <li><i className="fa fa-star-half-o"></i></li>
                                            <li><i className="fa fa-star-o"></i></li>
                                        </ul>
                                        <span>10 Review</span>
                                    </div>
                                </div>
                            </div>
                            <div className="dlab-info">
                                <h6 className="dlab-title"><a href="courses-details.html">Learn Python – Interactive Python Tutorial</a></h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                <div className="courses-info">
                                    <ul>
                                        <li><i className="fa fa-users"></i> 20 Student </li>
                                    </ul>
                                    <span className="price">$79.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}