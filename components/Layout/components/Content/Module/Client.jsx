import React, { Component } from 'react';

export default function Client() {
    return (
        <div className="section-full dlab-we-find bg-img-fix p-t20 p-b20 bg-white wow fadeIn" data-wow-duration="2s"data-wow-delay="0.6s">
            <div className="container">
                <div className="section-content">
                    <div
                        className="client-logo-carousel mfp-gallery gallery owl-btn-center-lr owl-carousel owl-btn-3">
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo1.jpg" alt=""/></a></div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo2.jpg" alt=""/></a></div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo1.jpg" alt=""/></a></div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo3.jpg" alt=""/></a></div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo4.jpg" alt=""/></a></div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="ow-client-logo">
                                <div className="client-logo"><a href="javascript:void(0);"><img
                                    src="images/client-logo/logo3.jpg" alt=""/></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}