import React, { Component } from 'react';

export default function Comment() {
    return (
        <div className="section-full overlay-black-middle bg-secondry content-inner-2 wow fadeIn"
                data-wow-duration="2s" data-wow-delay="0.2s"
                style={{backgroundImage: 'url(images/background/map-bg.png'}}>
            <div className="container">
                <div className="section-head text-white text-center">
                    <h2 className="title">What People Are Saying</h2>
                    <p>There are many variations of passages of Lorem Ipsum typesetting industry has been the
                        industry's standard dummy text ever since the been when an unknown printer.</p>
                </div>
                <div className="section-content">
                    <div
                        className="testimonial-two-dots owl-carousel owl-none owl-theme owl-dots-primary-full owl-loaded owl-drag">
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text ">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic3.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic2.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic1.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text ">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic3.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic2.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial-15 quote-right">
                                <div className="testimonial-text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the
                                        majority have suffered alteration in some form, by injected humour, or
                                        randomised words which don't look even slightly believable. If you are going
                                        to use a passage of Lorem Ipsum</p>
                                </div>
                                <div className="testimonial-detail clearfix">
                                    <div className="testimonial-pic radius">
                                        <img src="images/testimonials/pic1.jpg" width="100" height="100" alt=""/>
                                    </div>
                                    <strong className="testimonial-name">David Matin</strong> <span
                                    className="testimonial-position">Student</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}