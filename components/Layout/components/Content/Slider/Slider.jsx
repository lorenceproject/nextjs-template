import React, { Component } from 'react';

export default function Slider() {
    return (
        <div className="rev-slider">                
            <div id="rev_slider_1164_1_wrapper" className="rev_slider_wrapper fullscreen-container" data-alias="exploration-header" data-source="gallery" style={{ backgroundColor: 'transparent', padding: '0px' }}>
                <div id="rev_slider_1164_1" className="rev_slider fullscreenbanner" style={{ display : 'none'}} data-version="5.4.1">
                    <ul>
                        <li data-index="rs-3204" data-transition="slideoververtical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/main-slider/slide8.png"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="2000" data-fsslotamount="7" data-saveperformance="off"  data-title="" data-param1="What our team has found in the wild" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <img src="images/main-slider/slide8.png"  alt=""  data-lazyload="images/main-slider/slide8.png" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="6" className="rev-slidebg" data-no-retina/>
                            <div className="tp-caption tp-shape tp-shapewrapper"
                                id="slide-101-layer-14"
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                data-width="full"
                                data-height="full"
                                data-whitespace="nowrap"
                                data-type="shape"
                                data-basealign="slide"
                                data-responsive_offset="off"
                                data-responsive="off"
                                data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1500,"frame":"999","to":"opacity:0;","ease":"Power4.easeIn"}]'
                                data-textalign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style={{zIndex: '5', fontFamily: 'Open Sans', backgroundColor: 'rgba(0,0,0,0.15)', backgroundSize: '100%', backgroundRepeat: 'no-repeat', backgroundPosition: 'bottom'}}>
                            </div>
                            <div className="tp-caption"
                                id="slide-3204-layer-2"
                                data-x="['center','center','center','center']"
                                data-hoffset="['-210','-180','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['-80','-5','-180','-150']"
                                data-width="['700','600','600','260']"
                                data-fontsize="['60','45','30','24']"
                                data-lineheight="['70','45','36','30']"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-basealign="slide"
                                data-responsive_offset="off"
                                data-responsive="off"
                                data-textalign="['left','left','center','center']"
                                data-frames='[{"from":"y:50px;opacity:0;","speed":1500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style={{zIndex: '7', whiteSpace: 'normal', color: '#fff', fontFamily: 'Poppins, serif', borderWidth: '0px', fontWeight: '600', fontFamily: 'Merriweather, serif'}}>
                                Get Your Business To <br/>Scale Up Quickly
                            </div>
                            <div className="tp-caption tp-resizeme rs-parallaxlevel-1"
                                id="slide-100-layer-5"
                                data-x="['right','right','middle','middle']" data-hoffset="['-330','-400','0','0']"
                                data-y="['bottom','bottom','bottom','bottom']" data-voffset="['-40','-40','-20','-100']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="image"
                                data-responsive_offset="on"
                                data-frames='[{"delay":250,"speed":5000,"frame":"0","from":"y:50px;rZ:5deg;opacity:0;fb:50px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                                data-textalign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style={{ zIndex: '11' }}>
                            </div>
                            <div className="tp-caption  "
                                id="slide-3204-layer-3"
                                data-x="['center','center','center','center']"
                                data-hoffset="['-255','-230','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['50','40','-100','-60']"
                                data-width="['600','500','500','350']"
                                data-fontsize="['18','16','14','14']"
                                data-lineheight="['30','26','24','24']"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-basealign="slide"
                                data-responsive_offset="off"
                                data-responsive="off"
                                data-frames='[{"from":"y:50px;opacity:0;","speed":2000,"to":"o:1;","delay":750,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                                data-textalign="['left','left','center','center']"
                                data-paddingtop="[0,100,0,0]"
                                data-paddingright="[0,0,0,0]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[0,0,0,0]"
                                style={{ zIndex: '7', whiteSpace: 'normal', color: '#fff', fontFamily: 'Montserrat, sans-serif', borderWidth: '0px', fontWeight: '400'}}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the.
                            </div>
                            <a className="tp-caption rev-btn bg-primary btnhover13 tp-resizeme"
                                href="about-1.html" target="_blank"
                                id="slide-411-layer-13"
                                data-x="['center','center','center','center']"
                                data-hoffset="['-470','-400','-90','-70']"
                                data-y="['center','center','middle','middle']"
                                data-voffset="['150','180','-20','40']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="['nowrap','nowrap','nowrap','nowrap']"
                                data-type="button"
                                data-actions=''
                                data-basealign="slide"
                                data-responsive_offset="off"
                                data-responsive="off"
                                data-frames='[{"delay":"+690","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;","style":"c:rgba(255, 255, 255, 1.00);bg:var(--color-hover);"}]'
                                data-margintop="[0,0,0,0]"
                                data-marginright="[0,0,0,0]"
                                data-marginbottom="[0,0,0,0]"
                                data-marginleft="[0,0,0,0]"
                                data-textalign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[35,35,35,20]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[35,35,35,20]"
                                style={{ zIndex: '13', whiteSpace: 'normal', fontSize: '17px', lineHeight: '50px', fontWeight: '500', color: 'rgba(255, 255, 255, 1.00)', display: 'inline-block', fontFamily: 'Poppins', borderColor: 'rgba(255, 255, 255, 1.00)', borderStyle: 'solid', borderWidth: '0', borderRadius: '4px', outline: 'none', boxShadow: 'none', boxSizing: 'border-box', MozBoxSizing: 'border-box', WebkitBoxSizing: 'border-box', cursor: 'pointer', textDecoration: 'none' }}>Read More
                            </a>
                            <a className="tp-caption rev-btn tp-resizeme btnhover13"
                                href="about-1.html" target="_blank"
                                id="slide-411-layer-12"
                                data-x="['center','center','center','center']"
                                data-hoffset="['-290','-230','90','70']"
                                data-y="['center','center','middle','middle']"
                                data-voffset="['150','180','-20','40']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="['nowrap','nowrap','nowrap','nowrap']"
                                data-type="button"
                                data-actions=''
                                data-basealign="slide"
                                data-responsive_offset="off"
                                data-responsive="off"
                                data-frames='[{"delay":"+690","speed":2000,"frame":"0","from":"y:50px;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);"}]'
                                data-margintop="[0,0,0,0]"
                                data-marginright="[0,0,0,0]"
                                data-marginbottom="[0,0,0,0]"
                                data-marginleft="[0,0,0,0]"
                                data-textalign="['inherit','inherit','inherit','inherit']"
                                data-paddingtop="[0,0,0,0]"
                                data-paddingright="[34,34,34,19]"
                                data-paddingbottom="[0,0,0,0]"
                                data-paddingleft="[34,34,34,19]"
                                style={{ zIndex: '13', whiteSpace: 'normal', fontSize: '17px', lineHeight: '50px', fontWeight: '500', color: 'rgba(255, 255, 255, 1.00)', display: 'inline-block', fontFamily: 'Poppins', backgroundColor: 'rgba(255, 255, 255, 0)', borderColor: 'rgba(255, 255, 255, 1.00)', borderStyle: 'solid', borderWidth: '1px 1px 1px 1px', borderRadius: '4px', outline: 'none', boxShadow: 'none', boxSizing: 'border-box', MozBoxSizing: 'border-box', WebkitBoxSizing: 'border-box', cursor: 'pointer', textDecoration: 'none' }}>About Us
                            </a>
                        </li>
                    </ul>
                    <div className="tp-bannertimer tp-bottom" style={{ visibility: 'hidden !important'}}></div>
                </div>
            </div>                
        </div>
    );
}