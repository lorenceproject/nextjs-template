import React, { Component } from 'react';
import Top from './Top/Top';
import Bottom from './Bottom/Bottom';

export default function Footer() {
    return (
        <footer className="site-footer">
            <Top/>
            <Bottom/>
        </footer>
    );
}