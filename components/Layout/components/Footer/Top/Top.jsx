import React, { Component } from 'react';

export default function Top() {
    return (
        <div className="footer-top" style={{backgroundImage: 'url(images/pattern/pt15.png)' }}>
            <div className="container">
                <div className="row">
                    <div className="col-xl-3 col-lg-3 col-md-6 col-5 col-sm-6 footer-col-4 col-12">
                        <div className="widget widget_about border-0">
                            <h5 className="footer-title">About Us</h5>
                            <p className="mm-t5">Contrary to popular belief, Lorem simply random text. It has roots in a piece of classical Latin literature.</p>
                            <ul className="contact-info-bx">
                                <li><i className="las la-map-marker"></i><strong>Address</strong> 20 , New York 10010 </li>
                                <li><i className="las la-phone-volume"></i><strong>Phone</strong> 0800-123456</li>
                                <li><i className="las la-envelope-open"></i><strong>Email</strong> info@example.com</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-4 col-md-6 col-7 col-sm-6 footer-col-4 col-12">
                        <div className="widget border-0 recent-posts-entry">
                            <h5 className="footer-title">Latest Post</h5>
                            <div className="widget-post-bx">
                                <div className="widget-post clearfix">
                                    <div className="dlab-post-media">
                                        <img src="/images/blog/recent-blog/pic1.jpg" width="200" height="143" alt=""/>
                                    </div>
                                    <div className="dlab-post-info">
                                        <div className="dlab-post-header">
                                            <h6 className="post-title"><a href="blog-single.html">Helping you and your house become better.</a></h6>
                                        </div>
                                        <div className="dlab-post-meta">
                                            <ul>
                                                <li className="post-date"> <i className="la la-clock"></i> <strong>01 June</strong> <span> 2020</span> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="widget-post clearfix">
                                    <div className="dlab-post-media">
                                        <img src="/images/blog/recent-blog/pic2.jpg" width="200" height="160" alt=""/>
                                    </div>
                                    <div className="dlab-post-info">
                                        <div className="dlab-post-header">
                                            <h6 className="post-title"><a href="blog-single.html">Creating quality urban lifestyles.</a></h6>
                                        </div>
                                        <div className="dlab-post-meta">
                                            <ul>
                                                <li className="post-date"> <i className="la la-clock"></i> <strong>01 June</strong> <span> 2020</span> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-2 col-md-6 col-sm-6 footer-col-4 col-12">
                        <div className="widget widget_services border-0">
                            <h5 className="footer-title">Usefull Link</h5>
                            <ul className="mm-t10">
                                <li><a href="#!">About Us</a></li>
                                <li><a href="#!">Blog</a></li>
                                <li><a href="#!">Services</a></li>
                                <li><a href="#!">Privacy Policy</a></li>
                                <li><a href="#!">Projects </a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 footer-col-4 col-12">
                        <div className="widget">
                            <h5 className="footer-title">Opening Hours</h5>
                            <ul className="thsn-timelist-list mm-t5">
                                <li><span className="thsn-timelist-li-title">Mon – Tue</span><span className="thsn-timelist-li-value">10:00 – 18:00</span></li>
                                <li><span className="thsn-timelist-li-title">Wed – Thur</span><span className="thsn-timelist-li-value">10:00 – 17:00</span></li>
                                <li><span className="thsn-timelist-li-title">Fri – Sat</span><span className="thsn-timelist-li-value">10:00 – 12:30</span></li>
                                <li><span className="thsn-timelist-li-title">Saturday</span><span className="thsn-timelist-li-value">10:00 – 12:30</span></li>
                                <li><span className="thsn-timelist-li-title">Sunday</span><span className="thsn-timelist-li-value">Closed</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>       
        </div>
    );
}