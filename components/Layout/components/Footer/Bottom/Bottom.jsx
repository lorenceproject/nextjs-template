import React, { Component } from 'react';

export default function Bottom() {
    return (
        <div className="footer-bottom">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-sm-6 text-left "> <span>Copyright © 2020 DexignZone</span> </div>
                    <div className="col-md-6 col-sm-6 text-right ">
                        <div className="widget-link ">
                            <ul>
                                <li><a href="#!"> About</a></li>
                                <li><a href="#!"> Help Desk</a></li>
                                <li><a href="#!"> Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}