import React, { Component } from 'react';
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import Contact from './components/Contact/Contact'
import Content from './components/Content/Content'

export default function Layout() {
    return (
        <div className="page-wraper">
            <Header/>
            <Content/>
            <Contact/>
            <Footer/>
            <button className="scroltop icon-up" type="button"><i className="fa fa-arrow-up"></i></button>
        </div>
    );
}